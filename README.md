# Client Database Web App

This project is a full stack application that aims in assisting companies with the handling of their clients. The application displays the client list of a hypothetical organization on a webpage, with some client details (name, surname, telephone, address, email).
Through the webpage, the user (company employee) can make changes to the client list. They can add a new client, edit the details of a pre-existing one, or delete a client from the list. Additionally, the user can sort the clients displayed in the list alphabetically, based on their name, surname or email. Finally, the webpage can be accessed by a mobile device, and provide the same functions.

## Getting Started

In order to obtain a copy of the project you must clone the GitLab repository and open the project as a Maven project in any Java IDE.

### Prerequisites

Java version 8 or 11

Java IDE (e.g. IntelliJ)

### Installing

After cloning and opening the git project in an IDE wait until Maven changes are imported.

For Java 8:
* go in the pom.xml
* comment the following
    ```
    <properties>
		<java.version>11</java.version>
	</properties>
    ```
    
* uncomment the following
    ```
    <properties>
        <maven.compiler.target>1.8</maven.compiler.target>
        <maven.compiler.source>1.8</maven.compiler.source>
    </properties>
    ```

After this import the new Maven changes.

If the device you are using is already running another application on port 8080:
* go in the application.properties file
* uncomment the following
    ```
    server.port=8081
    ```

This will make the tomcat server run on port 8081.

Then run the program from the "ClientsWebAppApplication" class.

This line in the console will confirm that the web application is up and running:
```
2020-03-22 01:51:04.133  INFO 15660 --- [           main] c.c.c.ClientsWebAppApplication           : Started ClientsWebAppApplication in 2.195 seconds (JVM running for 3.049)
```

## Running the tests

After running the application, the user should open their browser and visit their localhost 8080 port (localhost:8080 or 127.0.0.1:8080). The user will then see the client list, filled with some demo data, pulled from the database. 
* To add a new client to the list, the user will have to press the "Create Client" button from the menu on the top of the webpage. An empty form will appear with the fields that are required to make a new entry in the list. Upon submitting the new clients details the user will be redirected to the full client list, and the new entry will be at the bottom of the list.
* To edit the details of an existing client, the user will need to press the "wrench" icon next to the specific client. The user will then be presented with a form which contains the details of that client. Upon completing and submitting the intended changes the user will be redirected to the client list webpages, where they can confirm that the changes were made.
* To delete a client from the list, the user needs to press the "x" button next to the client's details. The user will then be asked if they are certain that they want to delete that client, as well as warned that this action is irreversible.
* For the alphabetical sorting of clients based on name, surname, or email, the user will need to click on the name of each column. Clicking the name of the column once, will sort the clients in an ascending order, while clicking a second time will sort them in a descending order. For example, clicking on "Name" once will sort the clients alphabetically A-Z, while clicking a second time will sort them Z-A.
* To test the mobile responsiveness of the application the user will have to connect to the host's port 8080 with a mobile device and follow the above steps to test each functionality. The menu of the webpages is collapsed on mobile devices, and the user will have tu press on the button on the top right corner of the device to gain access to the "Client List" and "Create Client" buttons of the menu.

## Deployment

For the purposes of this project, an in-memory database was used (h2). However, for deployment in a live system, a traditional database should be used (e.g. MySQL) to provide the required persistence of the data. For this change to take place, a dependency must be added in the pom.xml file for the JDBC connector, and changes need to be made in the application.properties file, to provide the appropriate connection string for the database.

## Built With

* Java 11
* Spring Boot
* Maven
* Freemarker
* H2
* JPA

## Author

* **Anastasia Tsatsou**