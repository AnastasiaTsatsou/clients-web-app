package com.clientswebapp.clientswebapp.model;

public class ClientModel {
    private Long id;
    private String name;
    private String surname;
    private String telephone;
    private String address;
    private String email;

    public ClientModel() {
    }

    public ClientModel(String name, String surname, String telephone, String address, String email) {
        this.name = name;
        this.surname = surname;
        this.telephone = telephone;
        this.address = address;
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
