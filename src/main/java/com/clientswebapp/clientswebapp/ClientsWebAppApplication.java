package com.clientswebapp.clientswebapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClientsWebAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClientsWebAppApplication.class, args);
	}

}
