package com.clientswebapp.clientswebapp.controller;

import com.clientswebapp.clientswebapp.domain.Client;
import com.clientswebapp.clientswebapp.model.ClientModel;
import com.clientswebapp.clientswebapp.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class ClientController {

    @Autowired
    private ClientService clientService;

    @RequestMapping(value = {"","/","/clients"})
    public String getClients(Model model){
        List<Client> clients = clientService.getAllClients();
        model.addAttribute("clients", clients);
        return "list";
    }

    @GetMapping({"/create-client"})
    public String getClientCreatePage(Model model){
        Client newClient = new Client();
        model.addAttribute("client", newClient);
        return "create";
    }

    @PostMapping({"/create-client"})
    public String createNewClient(ClientModel model) {
        clientService.createClient(model);
        return "redirect:/clients";
    }

    @GetMapping({"/edit-client/{id}"})
    public String getClientEditPage(@PathVariable Long id, Model model){
        try{
            Client client = clientService.findClient(id);
            model.addAttribute("client", client);
            return "edit";
        }catch (Exception e){
            return "redirect:/clients";
        }
    }

    @PostMapping({"/edit-client/{id}"})
    public String returnFromClientEditPage(@PathVariable Long id, ClientModel model){
        Client client = clientService.findClient(id);

            clientService.updateClient(model);
            return "redirect:/clients";

    }

    @PostMapping({"/delete-client/{id}"})
    public String deleteClient(@PathVariable Long id){
        clientService.deleteById(id);
        return "redirect:/clients";
    }

}
