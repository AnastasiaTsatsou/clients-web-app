package com.clientswebapp.clientswebapp.mapper;

import com.clientswebapp.clientswebapp.domain.Client;
import com.clientswebapp.clientswebapp.model.ClientModel;

public class ClientModelToClientMapper {

    public static Client mapTo(ClientModel clientModel){
        Client client = new Client();
        client.setName(clientModel.getName());
        client.setSurname(clientModel.getSurname());
        client.setTelephone(clientModel.getTelephone());
        client.setAddress(clientModel.getAddress());
        client.setEmail(clientModel.getEmail());

        return client;
    }
}
