package com.clientswebapp.clientswebapp.service;

import com.clientswebapp.clientswebapp.domain.Client;
import com.clientswebapp.clientswebapp.mapper.ClientModelToClientMapper;
import com.clientswebapp.clientswebapp.model.ClientModel;
import com.clientswebapp.clientswebapp.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientServiceImpl implements ClientService {

    @Autowired
    private ClientRepository clientRepository;

    @Override
    public List<Client> getAllClients() {
        List<Client> clients = clientRepository.findAll();
        return clients;
    }

    @Override
    public Client findClient(Long id) {
        return clientRepository.findPropertyClientById(id).get();
    }

    @Override
    public Client createClient(ClientModel clientModel) {
        Client newClient = ClientModelToClientMapper.mapTo(clientModel);

        return clientRepository.save(newClient);
    }

    @Override
    public Client updateClient(ClientModel clientModel) {
        Client originalClient = ClientModelToClientMapper.mapTo(clientModel);

        originalClient.setId(clientModel.getId());

        return clientRepository.save(originalClient);
    }

    @Override
    public void deleteById(Long id) {
        clientRepository.deleteById(id);
    }
}
