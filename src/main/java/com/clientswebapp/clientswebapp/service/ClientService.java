package com.clientswebapp.clientswebapp.service;

import com.clientswebapp.clientswebapp.domain.Client;
import com.clientswebapp.clientswebapp.model.ClientModel;

import java.util.List;

public interface ClientService {

    List<Client> getAllClients();

    Client findClient(Long id);

    Client createClient(ClientModel clientModel);

    Client updateClient(ClientModel clientModel);

    void deleteById(Long id);
}
