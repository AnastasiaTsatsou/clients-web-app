package com.clientswebapp.clientswebapp.repository;

import com.clientswebapp.clientswebapp.domain.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {

    Optional<Client> findPropertyClientById(Long id);

    Client save(Client client);

    void deleteById(Long id);
}
