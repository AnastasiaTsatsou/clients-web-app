jQuery(function ($) {

	$('#client-create-validation').validate(
		{
			rules:
				{
					name: { required: true, maxlength: 255 },
					surname: { required: true, maxlength: 255 },
					telephone: {required: true, maxlength: 255 },
					address: {required: true, maxlength: 255 },
					email: { required: true, email: true, maxlength: 255},
				},
			messages:
				{
					name: { required: 'Please enter the client\'s first name', maxlength: 'The client\'s first name cannot have more than 255 characters'},
					surname: { required: 'Please enter the client\'s last name', maxlength: 'The client\'s last name cannot have more than 255 characters'},
					telephone: { required: 'Please enter the client\'s phone number', maxlength: 'The client\'s phone number cannot have more than 255 characters'},
					address: {required: 'Please enter the client\'s address', maxlength: 'The client\'s address cannot have more than 255 characters'},
					email: { required: 'Please enter the client\'s email', email: 'Please enter a valid email', maxlength: 'The client\'s email cannot have more than 255 characters'},
				}
		}
	);

	$('#client-edit-validation').validate(
		{
			rules:
				{
					name: { required: true, maxlength: 255 },
					surname: { required: true, maxlength: 255 },
					telephone: {required: true, maxlength: 255 },
					address: {required: true, maxlength: 255 },
					email: { required: true, email: true, maxlength: 255},
				},
			messages:
				{
					name: { required: 'Please enter the client\'s first name', maxlength: 'The client\'s first name cannot have more than 255 characters'},
					surname: { required: 'Please enter the client\'s last name', maxlength: 'The client\'s last name cannot have more than 255 characters'},
					telephone: { required: 'Please enter the client\'s phone number', maxlength: 'The client\'s phone number cannot have more than 255 characters'},
					address: {required: 'Please enter the client\'s address', maxlength: 'The client\'s address cannot have more than 255 characters'},
					email: { required: 'Please enter the client\'s email', email: 'Please enter a valid email', maxlength: 'The client\'s email cannot have more than 255 characters'},
				}
		}
	);


	$('#removeClientModal').on('show.bs.modal', function (event) {
		const id = event.relatedTarget.dataset.id;
		$('#deleteForm').attr('action', `/delete-client/${id}`);
		$('.modal-title').html('Delete Client');
		$('.modal-body').html('You are about to delete a client with id: '+id+'<br/>This action cannot be reversed.<br/>Do you want to proceed?');
	});

});